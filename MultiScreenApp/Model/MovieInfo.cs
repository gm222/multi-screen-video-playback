﻿namespace MultiScreenApp.Model
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class MovieInfo : DependencyObject, INotifyPropertyChanged, ICloneable
    {
        private ImageSource _thumbnail;
        private Uri _movieUrl;
        private TimeSpan _timeDuration;

        public MovieInfo ( )
        {

        }

        MediaPlayer player;
        private string _movieName;

        public MovieInfo ( string path, Stream stream, string fileName )
        {
            MovieSource = new Uri (path);
            MovieName = fileName;

            player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
            player.MediaOpened += OpenedTMPFile;
            player.Open (MovieSource);
        }

        private async void OpenedTMPFile ( object sender, EventArgs e )
        {
            if ( player.NaturalDuration.HasTimeSpan )
                MovieDuration = player.NaturalDuration.TimeSpan;
            try
            {
                player.Position = TimeSpan.FromSeconds (2);
                await Task.Delay(1000);
                player.Play ( );
                player.Pause ( );
            }
            catch ( Exception ) { }

            var width = player.NaturalVideoWidth;
            var height = player.NaturalVideoHeight;

            var rtb = new RenderTargetBitmap (width, height, 100, 100, PixelFormats.Pbgra32);
            var dv = new DrawingVisual ( );

            using ( DrawingContext dc = dv.RenderOpen ( ) )
                dc.DrawVideo (player, new Rect (0, 0, width, height));

            rtb.Render (dv);
            var bitmap = BitmapFrame.Create (rtb);
            player.Close ( );

            Thumbnail = bitmap;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged ( [CallerMemberName]string propertyName = null )
        {
            if ( PropertyChanged != null )
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }

        public void Remove ( object i )
        {
            throw new NotImplementedException ( );
        }

        public object Clone ( )
        {
            return MemberwiseClone();
        }

        public Uri MovieSource
        {
            get { return _movieUrl; }
            set { _movieUrl = value; OnPropertyChanged ( ); }
        }

        public String MovieName
        {
            get { return _movieName; }
            set { _movieName = value; OnPropertyChanged ( ); }
        }
        public TimeSpan MovieDuration
        {
            get { return _timeDuration; }
            set { _timeDuration = value; OnPropertyChanged ( ); }
        }

        public ImageSource Thumbnail
        {
            get
            {
                return _thumbnail;
            }

            set
            {
                _thumbnail = value;
                OnPropertyChanged ( );
            }
        }
    }
}
