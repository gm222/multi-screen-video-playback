﻿namespace MultiScreenApp.ViewModel
{
    public class ViewModelLocator
    {
        static ViewModelLocator ( )
        {

        }

        private MainViewModel _mainModel;

        public MainViewModel MainModel
        {
            get
            {
                return _mainModel ?? (_mainModel = new MainViewModel());
            }
        }

    }
}
