﻿namespace MultiScreenApp.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose ( )
        {
            this.OnDispose ( );
        }

        protected virtual void OnDispose ( ) { }

        protected virtual void OnPropertyChanged ( [CallerMemberName]string propertyName = null )
        {
            if ( PropertyChanged != null )
            {
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
            }
        }
    }
}
