﻿namespace MultiScreenApp.ViewModel
{
    public class PlayerViewModel : ViewModelBase
    {
        private int _id = 0;
        private MediaElementViewModel _currentMediaElement;

        public PlayerViewModel ( int id )
        {
            _id = id;
        }

        /// <summary>
        /// Sets and gets the CurrentMediaElement property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public MediaElementViewModel CurrentMediaElement {
            get {
                return _currentMediaElement ?? ( _currentMediaElement = new MediaElementViewModel (_id ) );
            }

            set {
                if ( _currentMediaElement != value ) {
                    _currentMediaElement = value;
                    OnPropertyChanged();
                }
                
            }
        }
    }
}
