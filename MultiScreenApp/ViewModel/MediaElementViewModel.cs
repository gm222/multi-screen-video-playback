﻿/*
This software uses MVVM Light

Copyright (c) GalaSoft Laurent Bugnion 2009 - 2016, laurent@galasoft.ch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

namespace MultiScreenApp.ViewModel
{
    using GalaSoft.MvvmLight.Messaging;
    using MultiScreenApp.Common;
    using MultiScreenApp.Model;
    using System;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Threading;

    public class MediaElementViewModel : ViewModelBase
    {
        #region Fields
        private int _nr = 0;
        private MediaElement _mediaPlayer;
        private PlayingState _playState = PlayingState.Stoped;
        private Uri _source;
        private MovieInfo _movie;
        DispatcherTimer timer;
        private string _textTime = "";

        #endregion
        #region Constructors
        public MediaElementViewModel ( )
        {
            TextTime = "No file selected...";
            timer = new DispatcherTimer ( );
            timer.Interval = TimeSpan.FromMilliseconds (250);
            timer.Tick += Timer_Tick;

            Messenger.Default.Register<PlayingState> (this, "ChangedInteraction", ChangePlayerState);
            Messenger.Default.Register<GroupContainerViewModel> (this, "MOVIE", SetMovie);
            Messenger.Default.Register<bool> (this, "MUTED", msg => MediaPlayer.IsMuted = msg);
            Messenger.Default.Register<object> (this, "ClearEvent", msg => ChangePlayerState (PlayingState.NotSet));
            Messenger.Default.Register<MovieInfo> (this, "MovieRemove", msg => { if ( msg == _movie ) ChangePlayerState (PlayingState.NotSet); });

            Binding bind = new Binding ("Source");
            bind.Source = this;

            MediaPlayer = new MediaElement ( )
            {
                LoadedBehavior = MediaState.Manual,
                Volume = 1f,
                Stretch = System.Windows.Media.Stretch.UniformToFill,
                IsMuted = false,
                ScrubbingEnabled = true
            };

            MediaPlayer.SetBinding (MediaElement.SourceProperty, bind);

            Play = MediaPlayer.Play;
            Stop = MediaPlayer.Stop;
            Pause = MediaPlayer.Pause;
            MediaPlayer.MediaEnded += MediaPlayer_MediaEnded;
        }
        public MediaElementViewModel ( int nr ) : this ( )
        {
            _nr = nr;
            Messenger.Default.Send<object[]> (new object[] { _nr, this }, "MediaRender");

        }
        #endregion
        #region Methods
        private void MediaPlayer_MediaEnded ( object sender, System.Windows.RoutedEventArgs e )
        {
            timer.Stop ( );
            string movieLenght = "00:00";
            if(_movie != null)
                movieLenght = _movie.MovieDuration.ToString (@"mm\:ss");
            TextTime = String.Format ("{0} / {0}",  movieLenght);
            Messenger.Default.Send<int> (_nr, "MovieEnded");
        }

        private void SetMovie ( GroupContainerViewModel obj )
        {
           timer.Stop();
            TextTime = String.Format ("{0} / {1}", "00:00", "00:00");
            var tmpMovie = _nr == 0 ? obj.FirstMovie : obj.SecondMovie;
            _mediaPlayer.Close();
            Source = null;
            _movie = tmpMovie;
            if ( _movie != null )
            {
                Source = _movie.MovieSource;
                TextTime = String.Format ("{0} / {1}", "00:00", _movie.MovieDuration.ToString (@"mm\:ss"));
                timer.Start ( );
                ChangePlayerState (PlayState);
            }
            else
            {
                _mediaPlayer.Close ( );
                _movie = null;
                Source = null;
            }

        }

        private void Timer_Tick ( object sender, EventArgs e )
        {
            if ( _mediaPlayer.Source != null && _movie != null)
                TextTime = String.Format ("{0} / {1}", _mediaPlayer.Position.ToString (@"mm\:ss"), _movie.MovieDuration.ToString (@"mm\:ss"));
            else
                TextTime = "No file selected...";
        }

        object _lock = new object ( );

        private void ChangePlayerState ( PlayingState state )
        {
            switch ( state )
            {
                case PlayingState.Playing:
                if ( Source != null )
                {
                    Play ( );
                    timer.Start ( ); //somthing wrong
                }
                break;
                case PlayingState.Paused:
                if ( Source != null ) Pause ( );
                break;
                case PlayingState.Stoped:
                if ( Source != null ) Stop ( );
                else
                    return;
                break;
                case PlayingState.NotSet:
                Stop ( );
                _mediaPlayer.Close ( );
                _movie = null;
                Source = null;
                MediaPlayer_MediaEnded(null, null);

                break;
            }

            PlayState = state;
        }
        #endregion
        #region Properties
        public string TextTime
        {
            get { return _textTime; }
            set { _textTime = value; OnPropertyChanged ( ); }
        }
        public PlayingState PlayState
        {
            get { return _playState; }
            set { _playState = value; }
        }
        public Uri Source
        {
            get { return _source; }
            set
            {
                _source = value;
                //ChangePlayerState(PlayState);
                OnPropertyChanged ( );
            }
        }
        public MediaElement MediaPlayer
        {
            get { return _mediaPlayer; }
            set { _mediaPlayer = value; OnPropertyChanged ( ); }
        }
        public Action Pause { get; private set; }
        public Action Play { get; private set; }
        public Action Stop { get; private set; }
        #endregion
    }
}
