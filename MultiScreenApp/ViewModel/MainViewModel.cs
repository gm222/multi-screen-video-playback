﻿/*
This software uses MVVM Light

Copyright (c) GalaSoft Laurent Bugnion 2009 - 2016, laurent@galasoft.ch

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



namespace MultiScreenApp.ViewModel
{
    using MultiScreenApp.Common;
    using MultiScreenApp.Model;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using MultiScreenApp.View;
    using System.Windows.Media;
    using System.Threading.Tasks;
    using System;
    public class MainViewModel : ViewModelBase
    {
        #region Constructros
        public MainViewModel ( )
        {

            _playersState = new PlayingState[2] { PlayingState.NotSet, PlayingState.NotSet };
            _screensList = new ObservableCollection<ScreenViewModel> ( );
            _fileDialog = new Microsoft.Win32.OpenFileDialog ( );
            _fileDialog.Filter = "MP4 files (*.mp4)|*.mp4|Avi Files (*.avi)|*.avi|All Video Formats(*.mp4;*.avi)|*.mp4;*.avi";
            _fileDialog.Multiselect = true;

            MovieList = new ObservableCollection<MovieInfo> ( );
            GroupList = new ObservableCollection<GroupContainerViewModel> ( );
            Messenger.Default.Register<int> (this, "MovieEnded", ( nr ) =>
            {
                _playersState[nr] = PlayingState.Stoped;
                MovieInteraction ( );
            });

            Messenger.Default.Register<object[]> (this, "MediaRender", MediaRenderSet);

        }

        #endregion
        #region Fields
        private bool _isAutoPlay;
        private ICommand _closingCommand;
        private ICommand _playCommand;
        private ICommand _pauseCommand;
        private ICommand _previousCommand;
        private ICommand _nextCommand;
        private ICommand _removeSelectedGroup;
        private ICommand _stopCommand;
        private ICommand _addVideoCommand;
        private ICommand _refreshCommand;
        private ICommand _addGroupCommand;
        private ICommand _clearMovieListCommand;
        private ICommand _clearRightListCommand;
        private ICommand _groupDoubleClickCommand;
        private ICommand _monitorDoubleClickCommand;
        private GroupContainerViewModel _selectedGroup;
        private ObservableCollection<MovieInfo> _movieList;
        private ObservableCollection<GroupContainerViewModel> _groupList;
        private ObservableCollection<ScreenViewModel> _screensList;
        private Microsoft.Win32.OpenFileDialog _fileDialog;
        private int _currentGroupAtCast = -1; //-1 - oznacza żadna grypa
        private ICommand _removeSelectedItem;
        private PlayingState[] _playersState;
        private MediaElementViewModel _media1;
        private MediaElementViewModel _media2;
        private bool _isMuted = false;
        private bool _playingState = false;

        #endregion
        #region Properties
        public bool IsMuted
        {
            get { return _isMuted; }
            set { _isMuted = value; Messenger.Default.Send<bool>(value, "MUTED"); OnPropertyChanged(); }
        }

        public bool PlayState
        {
            get { return _playingState; }
            set { _playingState = value; OnPropertyChanged ( ); }
        }
        public GroupContainerViewModel SelectedGroup
        {
            get { return _selectedGroup; }
            set { _selectedGroup = value; OnPropertyChanged ( ); }
        }
        public ICommand ClosingCommand
        {
            get { return _closingCommand ?? ( _closingCommand = new RelayCommand (CloseWindows) ); }
        }
        public ICommand RefreshCommand
        {
            get { return _refreshCommand ?? ( _refreshCommand = new RelayCommand (obj => SetMonitors ( )) ); }
        }
        public bool IsAutoPlay
        {
            get { return _isAutoPlay; }
            set
            {
                _isAutoPlay = value;
                OnPropertyChanged ( );
            }
        }
        #region Player Properties
        public ICommand PlayCommand
        {
            get { return _playCommand ?? ( _playCommand = new RelayCommand (msg => 
            {
                if ( CurrentGroupAtCast != -1 && CurrentGroupAtCast < GroupList.Count )
                {
                    var group = GroupList[CurrentGroupAtCast];
                    if ( group.FirstMovie != null )
                    _playersState[0] = PlayingState.Playing;
                    if ( group.SecondMovie != null )
                        _playersState[1] = PlayingState.Playing;
                    }
                Messenger.Default.Send<PlayingState> (PlayingState.Playing, "ChangedInteraction"); PlayState = true;
            }) ); }
        }
        public ICommand PreviousCommand
        {
            get { return _previousCommand ?? ( _previousCommand = new RelayCommand (PreviousGroup) ); }
        }
        public ICommand NextCommand
        {
            get { return _nextCommand ?? ( _nextCommand = new RelayCommand (NextGroup) ); }
        }
        public ICommand PauseCommand
        {
            get { return _pauseCommand ?? ( _pauseCommand = new RelayCommand (msg => { Messenger.Default.Send<PlayingState> (PlayingState.Paused, "ChangedInteraction"); PlayState = false; }) ); }
        }
        public ICommand StopCommand
        {
            get { return _stopCommand ?? ( _stopCommand = new RelayCommand (msg => { Messenger.Default.Send<PlayingState> (PlayingState.Stoped, "ChangedInteraction"); PlayState = false; }) ); }
        }
        public PlayerView P1 { get { return new PlayerView (0); } }
        public PlayerView P2 { get { return new PlayerView (1); } }
        #endregion
        #region Commands Properties
        public ICommand AddVideoCommand
        {
            get { return _addVideoCommand ?? ( _addVideoCommand = new RelayCommand (AddVideos) ); }
        }
        public ICommand GroupDoubleClickCommand
        {
            get { return _groupDoubleClickCommand ?? ( _groupDoubleClickCommand = new RelayCommand (ChangePlayedGroup) ); }
        }
        public ICommand MonitorDoubleClickCommand
        {
            get { return _monitorDoubleClickCommand ?? ( _monitorDoubleClickCommand = new RelayCommand (DisableScreen) ); }
        }
        public ICommand ClearRightListCommand
        {
            get { return _clearRightListCommand ?? ( _clearRightListCommand = new RelayCommand (ClearRightList) ); }
        }
        public ICommand ClearMovieListCommand
        {
            get { return _clearMovieListCommand ?? ( _clearMovieListCommand = new RelayCommand (ClearMovieList) ); }
        }
        #endregion
        #region Group Properties
        public ICommand AddGroupCommand
        {
            get { return _addGroupCommand ?? ( _addGroupCommand = new RelayCommand (AddGroupPanel) ); }
        }
        public ICommand RemoveSelectedItem
        {
            get { return _removeSelectedItem ?? ( _removeSelectedItem = new RelayCommand (RemoveSelectedItemGroup) ); }
        }
        public ICommand RemoveSelectedGroup
        {
            get { return _removeSelectedGroup ?? ( _removeSelectedGroup = new RelayCommand (RemoveGroup) ); }
        }
        public int CurrentGroupAtCast
        {
            get { return _currentGroupAtCast; }
            set { _currentGroupAtCast = value; if ( value != -1 && value < GroupList.Count ) SelectedGroup = GroupList[value]; else SelectedGroup = null; OnPropertyChanged ( ); } //tutaj jeszcze dodać inne rzeczy
        }
        #endregion
        #region List Properties
        public ObservableCollection<ScreenViewModel> ScreensList
        {
            get { return _screensList; }
        }
        public ObservableCollection<MovieInfo> MovieList
        {
            get { return _movieList; }
            set { _movieList = value; OnPropertyChanged ( ); }
        }
        public ObservableCollection<GroupContainerViewModel> GroupList
        {
            get { return _groupList; }
            set
            {
                _groupList = value;
                OnPropertyChanged ( );
            }
        }
        #endregion

        #endregion
        #region Methods

        private void MediaRenderSet ( object[] obj )
        {
            var id = (int)obj[0];

            switch ( id )
            {
                case 0:
                _media1 = obj[1] as MediaElementViewModel;
                break;
                case 1:
                _media2 = obj[1] as MediaElementViewModel;
                break;
            }

            if ( _media1 != null && _media2 != null )
                SetMonitors ( );
        }

        private void SetMonitors ( )
        {
            var tmpGroup = System.Windows.Forms.Screen.AllScreens.Where (x => !x.Primary).ToList ( );
            var tmp = System.Windows.Forms.Screen.AllScreens;

            if ( ScreensList.Count != 0 )
            {
                var result = tmpGroup.Where (p => !ScreensList.Any (l => p == l.Screen)).ToList ( );
                if ( result == null || result.Count == 0 ) return;

                var previousMonitor = ScreensList.Count % 2;

                for ( int i = 0; i < result.Count; i++ )
                {
                    ScreenViewModel screen = ( ( i + previousMonitor ) % 2 == 0 ) ? new ScreenViewModel (_media1, result[i]) : new ScreenViewModel (_media2, result[i]);
                    screen.ID = ScreensList.Count + 1;
                    ScreensList.Add (screen);
                }

            }
            else
            {
                for ( int i = 0; i < tmpGroup.Count; i++ )
                {
                    ScreenViewModel screen = ( i % 2 == 0 ) ? new ScreenViewModel (_media1, tmpGroup[i]) : new ScreenViewModel (_media2, tmpGroup[i]);
                    screen.ID = ScreensList.Count + 1;
                    ScreensList.Add (screen);
                }
            }


        }

        private void MovieInteraction ( )
        {
            for ( int i = 0; i < _playersState.Length; i++ )
                if ( _playersState[i] == PlayingState.Playing || _playersState[i] == PlayingState.Paused ) return;
            var oldValue = CurrentGroupAtCast;
            if ( IsAutoPlay && GroupList.Count > 1 )
            {
                GroupContainerViewModel group;
                try
                {
                    GroupList[CurrentGroupAtCast].GroupPlaying = false;
                    CurrentGroupAtCast++;
                    if ( CurrentGroupAtCast >= GroupList.Count ) CurrentGroupAtCast = 0;

                    group = GroupList[CurrentGroupAtCast];
                }
                catch ( Exception )
                {
                    if ( GroupList.Count == 0 || oldValue >= GroupList.Count ) return;
                    else if ( oldValue + 1 < GroupList.Count ) group = GroupList[oldValue + 1];
                    else group = GroupList[oldValue];
                }
                group.GroupPlaying = true;
                Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");

                if ( group.FirstMovie != null )
                    _playersState[0] = PlayingState.Playing;
                if ( group.SecondMovie != null )
                    _playersState[1] = PlayingState.Playing;
            }
            else
            {
                Messenger.Default.Send<PlayingState> (PlayingState.Stoped, "ChangedInteraction");
                PlayState = false;
            }

        }

        private void NextGroup ( object obj )
        {
            if ( CurrentGroupAtCast == -1 && GroupList.Count > 1 ) return;
            GroupList[CurrentGroupAtCast].GroupPlaying = false;
            CurrentGroupAtCast++;
            if ( CurrentGroupAtCast >= GroupList.Count ) CurrentGroupAtCast = 0;

            var group = GroupList[CurrentGroupAtCast];
            group.GroupPlaying = true;
            Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");
        }

        private void PreviousGroup ( object obj )
        {
            if ( CurrentGroupAtCast == -1 && GroupList.Count > 1 ) return;
            GroupList[CurrentGroupAtCast].GroupPlaying = false;
            CurrentGroupAtCast--;
            if ( CurrentGroupAtCast < 0 ) CurrentGroupAtCast = GroupList.Count - 1;

            var group = GroupList[CurrentGroupAtCast];
            group.GroupPlaying = true;
            Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");

        }

        object _lock = new object ( );

        private void ChangePlayedGroup ( object obj )
        {
            if ( CurrentGroupAtCast != -1 && CurrentGroupAtCast < GroupList.Count )
                GroupList[CurrentGroupAtCast].GroupPlaying = false;
            var group = obj as GroupContainerViewModel;
            group.GroupPlaying = true;
            CurrentGroupAtCast = GroupList.IndexOf (group);

            if ( group.FirstMovie != null )
                    _playersState[0] = PlayingState.Playing;
            if ( group.SecondMovie != null )
                _playersState[1] = PlayingState.Playing;

            Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");

        }

        private void DisableScreen ( object obj )
        {
            var screenViewModel = obj as ScreenViewModel;
            screenViewModel.IsEnabled = !screenViewModel.IsEnabled;
        }

        private void RemoveGroup ( object obj )
        {
            var group = obj as GroupContainerViewModel;
            if ( !group.GroupPlaying )
            {
                if ( CurrentGroupAtCast >= GroupList.Count && GroupList.Count == 0 ) CurrentGroupAtCast = -1;
                else if ( CurrentGroupAtCast - 1 >= 0 ) CurrentGroupAtCast--;
            }
            else
            {
                Messenger.Default.Send<MovieInfo> (group.FirstMovie, "MovieRemove");
                Messenger.Default.Send<MovieInfo> (group.SecondMovie, "MovieRemove");
            }
            GroupList.Remove (group);
            group.Dispose ( );
            group = null;

        }

        private void RemoveSelectedItemGroup ( object obj )
        {
            var movie = obj as MovieInfo;
            var group = GroupList.FirstOrDefault (x => x.FirstMovie == movie || x.SecondMovie == movie);

            if ( group != null )
            {
                if ( group.GroupPlaying )
                    Messenger.Default.Send<MovieInfo> (movie, "MovieRemove");

                if ( group.FirstMovie == movie )
                    group.FirstMovie = null;
                else
                    group.SecondMovie = null;
            }
        }

        private void AddGroupPanel ( object obj = null )
        {
            var group = new GroupContainerViewModel ( );
            //if(GroupList.Count == 0 )
            //{
            //    group.GroupPlaying = true;
            //    Messenger.Default.Send<PlayingState> (PlayingState.Stoped, "ChangedInteraction");
            //    PlayState = false;
            //    Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");
            //}

            GroupList.Add (group);
        }

        private void CloseWindows ( object obj = null )
        {
            for ( int i = 0; i < ScreensList.Count; i++ )
            {
                ScreensList[i].Dispose ( );
                ScreensList[i] = null;
            }

            ScreensList.Clear ( );
        }

        private void AddVideos ( object obj )
        {
            if ( _fileDialog.ShowDialog ( ) == true )
            {

                var catchedFilesCount = _fileDialog.FileNames.Length;
                var filesStream = _fileDialog.OpenFiles ( );
                var localMovieList = new MovieInfo[catchedFilesCount];

                for ( int i = 0; i < catchedFilesCount; i++ )
                {
                    MovieInfo movie = new MovieInfo (_fileDialog.FileNames[i], filesStream[i], _fileDialog.SafeFileNames[i]);
                    localMovieList[i] = movie;
                    MovieList.Add (movie);
                }

                bool result = false;
                int groupsCount = catchedFilesCount / 2;
                if ( catchedFilesCount % 2 == 1 ) groupsCount++;

                if ( obj != null && bool.TryParse (obj.ToString ( ), out result) && result == true )
                {
                    var tmpGroupList = new GroupContainerViewModel[groupsCount];

                    for ( int i = 0, j = 0; i < groupsCount && catchedFilesCount > 1; i += 2, j++ )
                        tmpGroupList[j] = new GroupContainerViewModel (localMovieList[i], localMovieList[i + 1]);

                    if ( catchedFilesCount % 2 == 1 )
                        tmpGroupList[groupsCount - 1] = new GroupContainerViewModel (localMovieList[catchedFilesCount - 1]);

                    if ( GroupList.Count == 0 )
                    {
                        var group = tmpGroupList[0];
                        group.GroupPlaying = true;
                        Messenger.Default.Send<PlayingState> (PlayingState.Stoped, "ChangedInteraction");
                        PlayState = false;
                        Messenger.Default.Send<GroupContainerViewModel> (group, "MOVIE");
                        CurrentGroupAtCast = 0;
                    }
                    //tmpGroupList[groupsCount - 1].FirstMovie = localMovieList[catchedFilesCount - 2];

                    //if ( catchedFilesCount % 2 == 0 )
                    //    tmpGroupList[groupsCount - 1].SecondMovie = localMovieList[catchedFilesCount - 1];

                    for ( int i = 0; i < groupsCount; i++ )
                        GroupList.Add (tmpGroupList[i]);
                }


                //if(CurrentMediaElement.PlayState == Common.PlayingState.Playing) CurrentMediaElement.Play();
            }
        }

        private void ClearRightList ( object obj = null )
        {
            //ChangePlayerState (PlayingState.NotSet);
            GroupListMovieRemove ( );

            for ( int i = 0; i < MovieList.Count; i++ )
                GroupList.Add (new GroupContainerViewModel ( ));
        }

        private void ClearMovieList ( object obj = null )
        {
            if ( _movieList.Count == 0 ) return;
            MovieList.Clear ( );
        }

        private void GroupListMovieRemove ( )
        {
            var length = GroupList.Count;

            for ( int i = 0; i < length; i++ )
            {
                GroupList[i].Dispose ( );
                GroupList[i] = null;
            }

            GroupList.Clear ( );
            Messenger.Default.Send<object> (null, "ClearEvent");
            CurrentGroupAtCast = -1;
        }

        #endregion
    }
}
