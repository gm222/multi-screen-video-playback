﻿namespace MultiScreenApp.ViewModel
{
    using MultiScreenApp.View;
    using System.Windows.Forms;

    public class ScreenViewModel : ViewModelBase
    {
        private bool _isEnabled;
        private System.Windows.Forms.Screen _screen;
        private ScreenView _view;
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged ( ); }
        }


        public ScreenViewModel ( MediaElementViewModel mediaPlayer, System.Windows.Forms.Screen screen )
        {
            MediaPlayer = mediaPlayer;
            _screen = screen;
            _view = new ScreenView ( );
            _view.DataContext = MediaPlayer;
            _view.Left = _screen.WorkingArea.Left;
            _view.Top = _screen.WorkingArea.Top;
            _view.Height = _screen.Bounds.Height;

            IsEnabled = true;
        }

        public MediaElementViewModel MediaPlayer { get; set; }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; HideOrActivate ( ); OnPropertyChanged ( ); }
        }

        public Screen Screen
        {
            get
            {
                return _screen;
            }
        }

        private void HideOrActivate ( )
        {
            if ( IsEnabled ) _view.Visibility = System.Windows.Visibility.Visible;
            else _view.Hide ( );
        }

        protected override void OnDispose ( )
        {
            base.OnDispose ( );
            _view.Hide();
            _view.Close ( );

            MediaPlayer = null;
            _screen = null;
            _view = null;

        }
    }
}
