﻿namespace MultiScreenApp.ViewModel
{
    using MultiScreenApp.Model;
    public class GroupContainerViewModel : ViewModelBase
    {
        private MovieInfo _firstMovie;
        private MovieInfo _secondMovie;

        public MovieInfo FirstMovie
        {
            get { return _firstMovie; }
            set { _firstMovie = value; OnPropertyChanged(); }
        }
        
        public MovieInfo SecondMovie
        {
            get { return _secondMovie; }
            set { _secondMovie = value; OnPropertyChanged(); }
        }

        private bool _isGroupPlaying = false;
        public bool GroupPlaying
        {
            get { return _isGroupPlaying; }
            set { _isGroupPlaying = value; OnPropertyChanged(); }
        }  

        public GroupContainerViewModel ( )
        {

        }

        public GroupContainerViewModel ( MovieInfo firstMovie, MovieInfo secondMovie = null )
        {
            FirstMovie = firstMovie;
            SecondMovie = secondMovie;
        }

        protected override void OnDispose ( )
        {
            base.OnDispose ( );
            this.FirstMovie = null;
            this.SecondMovie = null;
        }

       
    }
}
