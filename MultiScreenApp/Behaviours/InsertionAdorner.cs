/*
This software use source code under following license

The MIT License (MIT)

Copyright (c) 2013 Thomas Freudenberg

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows;

namespace MultiScreenApp.Behaviours
{
    public class InsertionAdorner : Adorner
    {
        private readonly bool _isSeparatorHorizontal;
        public bool IsInFirstHalf { get; set; }
        private readonly AdornerLayer _adornerLayer;
        private static readonly System.Windows.Media.Pen Pen;
        private static readonly PathGeometry Triangle;

        // Create the pen and triangle in a static constructor and freeze them to improve performance.
        static InsertionAdorner ( )
        {
            Pen = new System.Windows.Media.Pen { Brush = Brushes.Gray, Thickness = 2 };
            Pen.Freeze ( );

            var firstLine = new LineSegment (new Point (0, -5), false);
            firstLine.Freeze ( );
            var secondLine = new LineSegment (new Point (0, 5), false);
            secondLine.Freeze ( );

            var figure = new PathFigure { StartPoint = new Point (5, 0) };
            figure.Segments.Add (firstLine);
            figure.Segments.Add (secondLine);
            figure.Freeze ( );

            Triangle = new PathGeometry ( );
            Triangle.Figures.Add (figure);
            Triangle.Freeze ( );
        }

        public InsertionAdorner ( bool isSeparatorHorizontal, bool isInFirstHalf, UIElement adornedElement, AdornerLayer adornerLayer )
        : base (adornedElement)
        {
            this._isSeparatorHorizontal = isSeparatorHorizontal;
            this.IsInFirstHalf = isInFirstHalf;
            this._adornerLayer = adornerLayer;
            this.IsHitTestVisible = false;

            this._adornerLayer.Add (this);
        }

        // This draws one line and two triangles at each end of the line.
        protected override void OnRender ( DrawingContext drawingContext )
        {
            Point startPoint;
            Point endPoint;

            CalculateStartAndEndPoint (out startPoint, out endPoint);
            drawingContext.DrawLine (Pen, startPoint, endPoint);

            if ( this._isSeparatorHorizontal )
            {
                DrawTriangle (drawingContext, startPoint, 0);
                DrawTriangle (drawingContext, endPoint, 180);
            }
            else
            {
                DrawTriangle (drawingContext, startPoint, 90);
                DrawTriangle (drawingContext, endPoint, -90);
            }
        }

        private void DrawTriangle ( DrawingContext drawingContext, Point origin, double angle )
        {
            drawingContext.PushTransform (new TranslateTransform (origin.X, origin.Y));
            drawingContext.PushTransform (new RotateTransform (angle));

            drawingContext.DrawGeometry (Pen.Brush, null, Triangle);

            drawingContext.Pop ( );
            drawingContext.Pop ( );
        }

        private void CalculateStartAndEndPoint ( out Point startPoint, out Point endPoint )
        {
            startPoint = new Point ( );
            endPoint = new Point ( );

            double width = this.AdornedElement.RenderSize.Width;
            double height = this.AdornedElement.RenderSize.Height;

            if ( this._isSeparatorHorizontal )
            {
                endPoint.X = width;
                if ( !this.IsInFirstHalf )
                {
                    startPoint.Y = height;
                    endPoint.Y = height;
                }
            }
            else
            {
                endPoint.Y = height;
                if ( !this.IsInFirstHalf )
                {
                    startPoint.X = width;
                    endPoint.X = width;
                }
            }
        }

        public void Detach ( )
        {
            this._adornerLayer.Remove (this);
        }
    }
}