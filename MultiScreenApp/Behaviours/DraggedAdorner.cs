/*
This software use source code under following license

The MIT License (MIT)

Copyright (c) 2013 Thomas Freudenberg

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


*/


using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace MultiScreenApp.Behaviours
{
    public class DraggedAdorner : Adorner
    {
        private readonly ContentPresenter _contentPresenter;
        private double _left;
        private double _top;
        private readonly AdornerLayer _adornerLayer;

        public DraggedAdorner ( object dragDropData, DataTemplate dragDropTemplate, UIElement adornedElement, AdornerLayer adornerLayer )
        : base (adornedElement)
        {
            this._adornerLayer = adornerLayer;

            this._contentPresenter = new ContentPresenter
            {
                Content = dragDropData,
                ContentTemplate = dragDropTemplate,
                Opacity = 0.89f
            };

            this._adornerLayer.Add (this);
        }

        public void SetPosition ( double left, double top )
        {
            // -1 and +13 align the dragged adorner with the dashed rectangle that shows up
            // near the mouse cursor when dragging.
            this._left = left ;
            this._top = top ;
            if ( this._adornerLayer != null )
            {
                try
                {
                    this._adornerLayer.Update (this.AdornedElement);
                }
                catch { }
            }
        }

        protected override Size MeasureOverride ( Size constraint )
        {
            this._contentPresenter.Measure (constraint);
            return this._contentPresenter.DesiredSize;
        }

        protected override Size ArrangeOverride ( Size finalSize )
        {
            this._contentPresenter.Arrange (new Rect (finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild ( int index )
        {
            return this._contentPresenter;
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        public override GeneralTransform GetDesiredTransform ( GeneralTransform transform )
        {
            var result = new GeneralTransformGroup ( );
            result.Children.Add (base.GetDesiredTransform (transform));
            result.Children.Add (new TranslateTransform (this._left, this._top));

            return result;
        }

        public void Detach ( )
        {
            this._adornerLayer.Remove (this);
        }
    }
}