﻿namespace MultiScreenApp.View
{
    using MultiScreenApp.ViewModel;
    using System.Windows.Controls;


    /// <summary>
    /// Interaction logic for PlayerView.xaml
    /// </summary>
    public partial class PlayerView : UserControl
    {
        private int v;

        public PlayerView ( )
        {
            InitializeComponent ( );
            
        }

        public PlayerView ( int id ) : this()
        {
          DataContext = new PlayerViewModel(id);
        }
    }
}
